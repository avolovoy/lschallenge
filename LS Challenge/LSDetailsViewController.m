//
//  LSDetailsViewController.m
//  LS Challenge
//
//  Created by Alex Volovoy on 7/23/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

#import "LSDetailsViewController.h"
#import "LSCollectionViewDetailsCell.h"
#import "LSCollectionHeaderView.h"
#import "UIImageView+Networking.h"

@interface LSDetailsViewController ()
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewLayout;
@property (strong, nonatomic) LSCollectionViewDetailsCell *sizingCell;


@end

@implementation LSDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [self.feedItem objectForKey:@"title"];
    // Register CollectionView Cell
    UINib *headerNib = [UINib nibWithNibName:@"LSCollectionHeaderView" bundle:nil];
    [self.collectionView registerNib:headerNib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DetailsHeader"];
    UINib *cellNib = [UINib nibWithNibName:@"LSCollectionViewDetailsCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"LSCollectionViewDetailsCell"];
    self.sizingCell = [cellNib instantiateWithOwner:nil options:nil].firstObject;
    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

#pragma mark - UICollectionView Delegate
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    LSCollectionHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DetailsHeader" forIndexPath:indexPath];
    [header.imageView downloadImageWithUrl: [self.feedItem objectForKey:@"image_url"]];
    return header;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LSCollectionViewDetailsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LSCollectionViewDetailsCell" forIndexPath:indexPath];
    cell.feedItem = self.feedItem;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}


#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    self.sizingCell.feedItem = self.feedItem;
   
    CGFloat labelHight = self.sizingCell.descLabel.frame.size.height;
    CGSize cellSize = CGSizeMake(self.collectionView.bounds.size.width, labelHight + 25);
    
    return cellSize;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(self.collectionView.bounds.size.width, 200);
}



- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0f;
}


@end
