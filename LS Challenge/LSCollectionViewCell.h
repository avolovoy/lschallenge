//
//  LSCollectionViewCell.h
//  LS Challenge
//
//  Created by Alex Volovoy on 7/23/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) NSDictionary *feedItem;
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *merchantLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;



@end
