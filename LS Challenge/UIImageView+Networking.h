//
//  UIImageView+Networking.h
//  LS Challenge
//
//  Created by Alex Volovoy on 7/23/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Networking)

@property (nonatomic, strong) NSString *imageUrl;

-(void)downloadImageWithUrl:(NSString*)url;

@end
