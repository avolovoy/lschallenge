//
//  LSCollectionViewDetailsCell.m
//  LS Challenge
//
//  Created by Alex Volovoy on 7/23/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

#import "LSCollectionViewDetailsCell.h"

@implementation LSCollectionViewDetailsCell

- (void)awakeFromNib {
    // Initialization code
}

-(void) setFeedItem:(NSDictionary *)feedItem {
    self.title.text = [feedItem objectForKey:@"title"];
    self.merchantName.text = [feedItem objectForKey:@"merchant_name"];
    self.price.text = [NSString stringWithFormat:@"$%@", [feedItem valueForKey:@"price"] ];
    self.descLabel.text = [feedItem objectForKey:@"description"];
    [self.descLabel sizeToFit];
}


@end
