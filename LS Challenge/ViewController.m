//
//  ViewController.m
//  LS Challenge
//
//  Created by Alex Volovoy on 7/23/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

#import "ViewController.h"
#import "LSCollectionViewCell.h"
#import "LSDetailsViewController.h"

#define kFeedURL @"http://sheltered-bastion-2512.herokuapp.com/feed.json"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayout;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic) NSArray *feedItems;
@property (nonatomic) NSArray *filteredItems;
@property (nonatomic) NSURLSession *session;
@property (nonatomic) UIRefreshControl *refreshControl;
@property (nonatomic) BOOL filtering;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Living Social", nil);
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.feedItems = [NSArray new];
    self.filteredItems = [NSArray new];
    // Register CollectionView Cell
    UINib *cellNib = [UINib nibWithNibName:@"LSCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"LivingSocialCell"];
    // refresh control
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    // Refresh Data
    [self configureNetworkSession];
    [self refreshData];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.filtering ? self.filteredItems.count : self.feedItems.count;
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

#pragma mark - UICollectionView Delegate

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LivingSocialCell" forIndexPath:indexPath];
    NSDictionary *feedItem = self.filtering ? self.filteredItems[indexPath.item] : self.feedItems[indexPath.item];
    cell.feedItem = feedItem;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *feedItem = self.filtering ? self.filteredItems[indexPath.item] : self.feedItems[indexPath.item];
    LSDetailsViewController *detailsController = [self.storyboard instantiateViewControllerWithIdentifier:@"LSDetailsViewController"];
    detailsController.feedItem = feedItem;
    [self.navigationController pushViewController:detailsController animated:YES];
    
}


#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((self.collectionView.bounds.size.width-1)/2, 300);
}



- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0f;
}

#pragma mark - Search Feed Items

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    if ( searchText.length > 0) {
        self.filtering = YES;
        self.filteredItems = [self.feedItems filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(title CONTAINS[cd] %@)", searchText]];
        [self.collectionView reloadData];
    } else {
        self.filtering = NO;
        self.filteredItems = [NSArray new];
        [self.searchBar resignFirstResponder];
        [self.collectionView reloadData];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    self.filtering = NO;
    self.filteredItems = [NSArray new];
    [self.searchBar resignFirstResponder];
    [self.collectionView reloadData];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *) searchBar {
    [self.searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *) searchBar {
    [self.searchBar setShowsCancelButton:YES animated:YES];

}

- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    self.filtering = NO;
    self.filteredItems = [NSArray new];
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchBar resignFirstResponder];

}



#pragma mark - Network requests

-(void)configureNetworkSession
{
    NSURLSessionConfiguration *defaultConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.session = [NSURLSession sessionWithConfiguration: defaultConfig delegate:nil delegateQueue: [NSOperationQueue mainQueue]];
    
}

-(void) refreshData
{
    
    NSLog(@"Loading Items");
    self.navigationItem.rightBarButtonItem.enabled = NO;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.refreshControl beginRefreshing];
    
    NSURL *URL = [NSURL URLWithString:kFeedURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      NSLog(@"%@", response);
                                      //Lets accept only 200 for simplicity
                                      if ([(NSHTTPURLResponse *) response  statusCode] == 200) {
                                          NSArray *json = [NSJSONSerialization
                                                           JSONObjectWithData:data
                                                           options:kNilOptions
                                                           error:&error];
                                          if (!error) {
                                              if (json) {
                                                  self.feedItems = [NSArray arrayWithArray:json];
                                                  [self.collectionViewFlowLayout invalidateLayout];
                                                  [self.collectionView reloadData];
                                              }
                                          }
                                      }
                                     
                                     
                                      self.navigationItem.rightBarButtonItem.enabled = YES;
                                      [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                      [self.refreshControl endRefreshing];
                                      
                                  }];
    
    [task resume];
}


@end
