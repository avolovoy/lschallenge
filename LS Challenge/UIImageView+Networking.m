//
//  UIImageView+Networking.m
//  LS Challenge
//
//  Created by Alex Volovoy on 7/23/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

#import "UIImageView+Networking.h"
#import <objc/runtime.h>


NSString const *key = @"lcimageurlkey";


@implementation UIImageView (Networking)


- (void)setImageUrl:(NSString *)imageUrl {
    objc_setAssociatedObject(self, &key, imageUrl, OBJC_ASSOCIATION_COPY);
}

- (NSString *)imageUrl {
    return objc_getAssociatedObject(self, &key);
}

-(void)downloadImageWithUrl:(NSString*)url {
    self.imageUrl = url;
    if (url) {
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDownloadTask *imageTask =
        [session downloadTaskWithURL:[NSURL URLWithString:url]
                   completionHandler:^(NSURL *url,
                                       NSURLResponse *response,
                                       NSError *error) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           if ( [self.imageUrl isEqualToString:[response.URL absoluteString]]) {
                                UIImage *downloadedImage =  [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                               self.image = downloadedImage;
                           }
                           
                       });
                   }];
        [imageTask resume];
        
    }
}

@end

