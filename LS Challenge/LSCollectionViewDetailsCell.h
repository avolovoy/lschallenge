//
//  LSCollectionViewDetailsCell.h
//  LS Challenge
//
//  Created by Alex Volovoy on 7/23/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSCollectionViewDetailsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *merchantName;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) NSDictionary *feedItem;

@end
