//
//  LSCollectionHeaderView.h
//  LS Challenge
//
//  Created by Alex Volovoy on 7/23/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSCollectionHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
