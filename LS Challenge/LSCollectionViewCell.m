//
//  LSCollectionViewCell.m
//  LS Challenge
//
//  Created by Alex Volovoy on 7/23/15.
//  Copyright (c) 2015 Alexey Volovoy. All rights reserved.
//

#import "LSCollectionViewCell.h"
#import "UIImageView+Networking.h"


@implementation LSCollectionViewCell

//"description":"Quidem nemo accusantium magni. Ex rem suscipit consequatur est animi. Unde voluptas voluptas vel. Consequatur est explicabo unde saepe. Quis repellendus occaecati veritatis inventore provident. Repellendus explicabo quidem exercitationem voluptas. Corrupti et molestias soluta consequatur impedit culpa. Eum ab magnam sed qui. Nemo libero a quo quia. Illo voluptas qui iure unde quas corporis. Nostrum aut in distinctio sapiente. Hic omnis minima repudiandae.",
//"image_url":"http://robohash.org/ratione-numquam.png?size=500x500",
//"merchant_name":"Parker and Sons",
//"price": 90.75,
//"title":"Incredible Rubber Table"

- (void)awakeFromNib {
    // Initialization code
}

-(void)prepareForReuse {
    [super prepareForReuse];
    self.feedItem = nil;
    self.itemImageView.image = nil;
    self.titleLabel.text = @"";
    self.merchantLabel.text = @"";
    self.priceLabel.text = @"";
}

-(void) setFeedItem:(NSDictionary *)feedItem {
    self.titleLabel.text = [feedItem objectForKey:@"title"];
    self.merchantLabel.text = [feedItem objectForKey:@"merchant_name"];
    [self.itemImageView downloadImageWithUrl:[feedItem objectForKey:@"image_url"]];
    self.priceLabel.text = [NSString stringWithFormat:@"$%@", [feedItem valueForKey:@"price"] ];

}


@end
